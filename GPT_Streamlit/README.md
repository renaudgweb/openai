# Chatbot OpenAI GPT-3 + Streamlit

This code uses the OpenAI Completion API to generate responses based on user requests. It uses Streamlit to create a user interface to interact with the chatbot.

## Usage

1. Obtain an OpenAI API key and add it to the code by replacing "API-KEY-HERE".
2. Run the code using the command `streamlit run app.py`.
3. Enter a request in the text area and press Enter to generate a response.

## Features

The chatbot uses the `generate_response(prompt)` function to generate a response based on the given prompt. The temperature controls the degree of randomness of the generated response.

The `get_text()` function allows the user to enter a request. User inputs and generated responses are stored in session variables `past` and `generated`.

Messages are displayed to the user using the `message()` function from the `streamlit_chat` module.

## Usage Limits

It is important to note that the use of the OpenAI API is limited in terms of the number of requests allowed in a given period. Users should be aware of these limits to avoid any abusive use of the API.

## Contributing

Contributions are welcome! External developers are invited to contribute to the code by submitting pull requests.

## License

This code is licensed under the MIT license. Please refer to the `LICENSE` file for more information.

----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------

# Chatbot OpenAI GPT-3 + Streamlit

Ce code utilise l'API OpenAI Completion pour générer des réponses en fonction des demandes de l'utilisateur. Il utilise Streamlit pour créer une interface utilisateur pour dialoguer avec le chatbot.

## Utilisation

1. Obtenez une clé API OpenAI et ajoutez-la dans le code en remplaçant "API-KEY-HERE".
2. Exécutez le code à l'aide de la commande `streamlit run app.py`.
3. Saisissez une demande dans la zone de texte et appuyez sur Entrée pour générer une réponse.

## Fonctionnalités

Le chatbot utilise la fonction `generate_response(prompt)` pour générer une réponse en fonction du prompt donné. La température contrôle le degré d'aléatoire de la réponse générée.

La fonction `get_text()` permet à l'utilisateur de saisir une demande. Les entrées de l'utilisateur et les réponses générées sont stockées dans des variables de session `past` et `generated`.

Les messages sont affichés à l'utilisateur à l'aide de la fonction `message()` du module `streamlit_chat`.

## Limites d'utilisation

Il est important de noter que l'utilisation de l'API OpenAI est limitée en termes de nombre de requêtes autorisées dans une période donnée. Les utilisateurs doivent être conscients de ces limites pour éviter toute utilisation abusive de l'API.

## Contribuer

Les contributions sont les bienvenues ! Les développeurs externes sont invités à contribuer au code en soumettant des pull requests.

## Licence

Ce code est sous licence MIT. Veuillez consulter le fichier `LICENSE` pour plus d'informations.
