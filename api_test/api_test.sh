#!/bin/bash
#
# Programme pour générer du texte à partir d'un prompt en utilisant l'API
# OpenAI.
#
# Auteur: Renaud Gajda
# Date de création: 20/03/2023
# Version: 0.2
#
# Prérequis :
#  - Un compte OpenAI et une clé d'API valide.
#  - cURL
#  - jq
#
# Utilisation: ./api_test.sh
#

while true; do
	read -p "🧑‍💻️You: " user_input
	echo "🤖GPT3.5: $(curl https://api.openai.com/v1/chat/completions \
		-H "Content-Type: application/json" \
		-H "Authorization: Bearer $OPENAI_API_KEY" \
		-d '{
			"messages": [
				{"role": "system", "content": "You are a text-specialized AI, fluent in French or English."},
				{"role": "assistant", "content": "Understood, I am a text-specialized AI, fluent in French or English."},
				{"role": "user", "content": "Analyze the essential information and details from the input text to generate a comprehensible response. Respond in the language of the input text."},
				{"role": "assistant", "content": "I will analyze the input text while preserving the essential information and details to generate a comprehensible response. I will respond in the language of the input text."},
				{"role": "user", "content": "'"$user_input"'"}
			],
			"model": "gpt-3.5-turbo-0125",
			"temperature": 0.8,
        	"frequency_penalty": 1,
        	"presence_penalty": 1
		}' | jq -r '.choices[0].message.content' )"
done
