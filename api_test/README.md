# OpenAI API

## Introduction
Ce programme utilise l'API OpenAI pour générer des réponses à partir d'un modèle GPT-3.5. Il permet de simuler une conversation avec une IA.

## Prérequis
Pour utiliser ce programme, vous devez disposer d'une clé API OpenAI valide. Vous pouvez vous inscrire sur le site openai.com pour en obtenir une.

## Installation
Ce programme ne nécessite aucune installation particulière. Vous devez simplement vous assurer que vous avez Python 3 et le module OpenAI installé.

## Utilisation
Pour lancer le programme, exécutez le fichier `api_test.py` à partir de votre terminal ou de votre IDE Python. Le programme vous demandera de saisir un texte. Vous pouvez saisir n'importe quelle entrée pour commencer la conversation avec l'IA.

Le programme affichera ensuite la réponse générée par le modèle GPT-3.5. La conversation se poursuivra jusqu'à ce que vous décidiez de la terminer.

## Licence
Ce programme est distribué sous licence MIT. Vous êtes libre de l'utiliser, de le modifier et de le distribuer selon vos besoins
