#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
OpenAI Chat API Test

Programme pour générer du texte à partir d'un prompt en utilisant l'API
OpenAI.

Ce programme utilise l'API OpenAI pour générer du texte à partir d'un prompt
donné. L'API OpenAI est une interface de programmation qui permet aux
développeurs d'utiliser les modèles d'IA de GPT-3 pour effectuer différentes
tâches de traitement du langage naturel.

Auteur : Renaud Gajda
Date : 20/03/2023
Version : 0.2

Prérequis :
- Un compte OpenAI et une clé d'API valide.
- Le module 'openai' pour Python.
"""


import os
import sys
import openai

openai.api_key = "API-KEY-HERE"


def generate_response(input_text):
	response = openai.ChatCompletion.create(
		messages=[
			{"role": "system", "content": "You are a text-specialized AI, fluent in French or English."},
			{"role": "assistant", "content": "Understood, I am a text-specialized AI, fluent in French or English."},
			{"role": "user", "content": "Analyze the essential information and details from the input text to generate a comprehensible response. Respond in the language of the input text."},
			{"role": "assistant", "content": "I will analyze the input text while preserving the essential information and details to generate a comprehensible response. I will respond in the language of the input text."},
			{"role": "user", "content": f"Here is the input text : {input_text}"},
		],
		model="gpt-3.5-turbo-0125",
		temperature=0.8,
        frequency_penalty=1,
        presence_penalty=1
	)

	return response.choices[0].message.content


def main():
	while True:
		user_input = input("🧑‍💻️You: ")
		response = generate_response(user_input)
		print(f"🤖️GPT3.5: {response}")


if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print('\n -> Interrupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

