# Utilisation de l'API OpenAI

Le dossier api_test contient deux fichiers : 

- **Python** : Contient le fichier Python pour tester l'API OpenAI
- **Bash** : Contient le fichier Bash pour tester l'API OpenAI

## Prérequis

- Python 3.6 ou supérieur
- Un compte OpenAI avec une clé d'API valide

## Configuration

### Clé API OpenAI

Pour pouvoir utiliser l'API OpenAI, vous devez avoir une clé d'API valide. Si vous n'en avez pas, vous pouvez en créer une en suivant les instructions de la documentation officielle d'OpenAI.

Une fois que vous avez une clé d'API, vous devez la copier dans un fichier `.env` à la racine du dossier **Python**. Le fichier doit avoir le format suivant :

```
OPENAI_API_KEY=<votre_clé_d'API>
```

### Installation des dépendances

Avant de pouvoir utiliser les fichiers Python ou Bash, vous devez installer les dépendances nécessaires. Pour ce faire, exécutez la commande suivante à la racine du dossier :

```
pip install -r requirements.txt
```

## Utilisation

### Python

Le fichier **Python** contient un fichier pour tester la fonctionnalité Chat de l'API OpenAI. Les fichiers sont nommés en fonction de la fonctionnalité testée.

Pour utiliser un fichier de test, exécutez la commande suivante à la racine du dossier **Python** :

```
python3 app.py
```

### Bash

Le fichier **Bash** contient un script pour tester la fonctionnalité Chat de l'API OpenAI. Les scripts sont nommés en fonction de la fonctionnalité testée.

Pour utiliser un script de test, exécutez la commande suivante à la racine du dossier **Bash** :

```
./api_test.sh
```

## Application Streamlit

Le dossier **chatbotGPT** contient une application Python utilisant l'API OpenAI et Streamlit. L'application permet de générer du texte à partir d'une phrase d'entrée.

Pour exécuter l'application, exécutez la commande suivante à la racine du dossier **streamlit_app** :

```
streamlit run app.py
```

## Avertissement

Ce dépôt est susceptible d'évoluer en fonction des tests à venir. Les noms des fichiers et les fonctionnalités testées peuvent changer à tout moment. Consultez régulièrement le dépôt pour vous assurer d'utiliser la dernière version.
