<?php
    $ip = $_SERVER["REMOTE_ADDR"];

    $openai_api_key = "API-KEY-HERE";
    $messages = array();

    $config = file_get_contents("config.json");
    $configData = json_decode($config, true);

    $model = $configData["model"];
    $temperature = $configData["temperature"];
    $frequency = $configData["frequency_penalty"];
    $presence = $configData["presence_penalty"];

    function generate_response($input_text) {
        global $openai_api_key;

        global $model;
        global $temperature;
        global $frequency;
        global $presence;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.openai.com/v1/chat/completions",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(array(
                "messages" => array(
                    array("role" => "system", "content" => "You are a helpful assistant."),
                    array("role" => "user", "content" => $input_text)
                ),
                "model" => $model,
                "temperature" => $temperature,
                "frequency_penalty" => $frequency,
                "presence_penalty" => $presence
            )),
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer $openai_api_key",
                "content-type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
          $response_data = json_decode($response, true);
          $response_message = $response_data["choices"][0]["message"]["content"];
          $response_prompt = $response_data["usage"]["prompt_tokens"];
          $response_completion = $response_data["usage"]["completion_tokens"];
          $response_total = $response_data["usage"]["total_tokens"];
          $response_created = $response_data["created"];
          $date_message = date("D M j G:i:s T Y", $response_created);
          return array(
              "message" => $response_message,
              "prompt" => $response_prompt,
              "completion" => $response_completion,
              "total" => $response_total,
              "created" => $date_message
          );
        }
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        header("Content-Type: application/json");

        if(isset($_POST["user_input"]) && !empty($_POST["user_input"])) {
          $user_input = htmlspecialchars($_POST["user_input"]);
          $messages[] = array("sender" => "YOU", "content" => $user_input);
          $response = generate_response($user_input);
          $messages[] = array("sender" => "GPT", "content" => $response["message"]);
          $prompt = $response["prompt"];
          $completion = $response["completion"];
          $total = $response["total"];
          $date_creation = $response["created"];

          echo json_encode(array("messages" => $messages,"prompt" => $prompt, "completion" => $completion, "total" => $total, "created" => $date_creation));
        } else {
            echo json_encode(array("error" => "Prompt empty"));
        }

        exit();
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0, shrink-to-fit=no"/>
    <meta name="theme-color" content="#001400" media="(prefers-color-scheme: light)">
    <meta name="theme-color" content="#001400" media="(prefers-color-scheme: dark)">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" type="image/png" href="favicon.ico">
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="contents/css/styles.css"/>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"></script>

    <title>GPT-87</title>
</head>
<body class="body-green">
    <audio controls preload="auto">
        <source src="contents/sounds/blip.mp3" type="audio/mpeg">
        <source src="contents/sounds/click.mp3" type="audio/mpeg">
        <source src="contents/sounds/computer-processing.mp3" type="audio/mpeg">
    </audio>
    <form action="update_json.php" id="configForm" method="post">
        <div class="conf conf-select">
            <select name="model" id="model" class="model-green">
                <option value="gpt-3.5-turbo" selected="yes">gpt-3.5-turbo</option>
                <option value="gpt-3.5-turbo-instruct">gpt-3.5-turbo-instruct</option>
                <option value="gpt-4-turbo-preview">gpt-4-turbo</option>
                <option value="gpt-4">gpt-4</option>
            </select>
            <label for="model"><span class="infoBulle" title="gpt-3.5-turbo: Most capable GPT-3.5 model and optimized for chat at 1/10th the cost of text-davinci-003. Will be updated with our latest model iteration.
Max Tokens: 4,096 tokens.
Training Data: Up to Sep 2021
Modèle GPT-3.5 le plus performant et optimisé pour les conversations à 1/10e du coût de text-davinci-003. Sera mis à jour avec notre dernière itération de modèle.
Nombre maximal de jetons : 4 096 jetons.
Données d'entraînement : Jusqu'en septembre 2021

gpt-3.5-turbo-16k: Same capabilities as the standard gpt-3.5-turbo model but with 4 times the context.
Max Tokens: 16,384 tokens.
Training Data: Up to Sep 2021
Mêmes capacités que le modèle standard gpt-3.5-turbo mais avec 4 fois plus de contexte.
Nombre maximum de tokens : 16 384 tokens.
Données d'entraînement : jusqu'en septembre 2021

gpt-4: More capable than any GPT-3.5 model, able to do more complex tasks, and optimized for chat. Will be updated with our latest model iteration.
Max Tokens: 8,192 tokens.
Training Data: Up to Sep 2021
(Not available now)
Plus performant que tout modèle GPT-3.5, capable d'effectuer des tâches plus complexes et optimisé pour les conversations. Sera mis à jour avec notre dernière itération de modèle.
Nombre maximal de jetons : 8 192 jetons.
Données d'entraînement : Jusqu'en septembre 2021

gpt-4-32k: Same capabilities as the standard gpt-4 model but with 4 times the context.
Max Tokens: 32,768 tokens.
Training Data: Up to Sep 2021
Mêmes capacités que le modèle standard gpt-4 mais avec 4 fois plus de contexte.
Nombre maximum de tokens : 32,768 tokens.
Données d'entraînement : jusqu'en septembre 2021

https://platform.openai.com/docs/models/gpt-4">Modele: </span></label>
            <output class="model-output" for="model"><?php echo $model; ?></output>
        </div>
        <div class="conf">
            <label for="temperature"><span class="infoBulle" title="Temperature: What sampling temperature to use, between 0 and 2. Higher values like 0.8 will make the output more random, while lower values like 0.2 will make it more focused and deterministic.

Température : quelle température d'échantillonnage utiliser, entre 0 et 2. Des valeurs plus élevées comme 0,8 rendront la sortie plus aléatoire, tandis que des valeurs plus basses comme 0,2 la rendront plus ciblée et déterministe.">Temperature: </span></label>
            <input class="input-conf" type="range" name="temperature" id="temperature" min="0" max="2" step="0.01" value="<?php echo $temperature; ?>">
            <output class="temperature-output" for="temperature"><?php echo $temperature; ?></output>
        </div>
        <div class="conf">
            <label for="frequencyPenalty"><span class="infoBulle" title="Frequency_penalty: Number between -2.0 and 2.0. Positive values penalize new tokens based on their existing frequency in the text so far, decreasing the model's likelihood to repeat the same line verbatim.

Frequency_penalty : Nombre compris entre -2.0 et 2.0. Les valeurs positives pénalisent les nouveaux jetons en fonction de leur fréquence existante dans le texte jusqu'à présent, réduisant ainsi la probabilité du modèle de répéter exactement la même phrase.">FrequenceP: </span></label>
            <input class="input-conf" type="range" name="frequencyPenalty" id="frequencyPenalty" min="-2" max="2" step="0.01" value="<?php echo $frequency; ?>">
            <output class="frequencyPenalty-output" for="frequencyPenalty"><?php echo $frequency; ?></output>
        </div>
        <div class="conf">
            <label for="presencePenalty"><span class="infoBulle" title="Presence_penalty: Number between -2.0 and 2.0. Positive values penalize new tokens based on whether they appear in the text so far, increasing the model's likelihood to talk about new topics.

Presence_penalty : Nombre compris entre -2.0 et 2.0. Les valeurs positives pénalisent les nouveaux jetons en fonction de leur présence dans le texte jusqu'à présent, augmentant ainsi la probabilité du modèle de parler de nouveaux sujets.">PresenceP: </span></label>
            <input class="input-conf" type="range" name="presencePenalty" id="presencePenalty" min="-2" max="2" step="0.01" value="<?php echo $presence; ?>">
            <output class="presencePenalty-output" for="presencePenalty"><?php echo $presence; ?></output>
        </div>
    </form>
    <div id="terminal" class="term-green">
    <p class="version">v2.1.7</p>
        <h1>GPT-87</h1>
        <pre>
     ______  _______  ________         ______  ________
     /      \|       \|        \       /      \|        \
    |  ▓▓▓▓▓▓\ ▓▓▓▓▓▓▓\\▓▓▓▓▓▓▓▓      |  ▓▓▓▓▓▓\\▓▓▓▓▓▓▓▓
   | ▓▓ __\▓▓ ▓▓__/ ▓▓  | ▓▓   ______| ▓▓__/ ▓▓   /  ▓▓
  | ▓▓|    \ ▓▓    ▓▓  | ▓▓  |      \>▓▓    ▓▓  /  ▓▓
 | ▓▓ \▓▓▓▓ ▓▓▓▓▓▓▓   | ▓▓   \▓▓▓▓▓▓  ▓▓▓▓▓▓  /  ▓▓
| ▓▓__| ▓▓ ▓▓        | ▓▓         | ▓▓__/ ▓▓/  ▓▓
\▓▓    ▓▓ ▓▓        | ▓▓          \▓▓    ▓▓  ▓▓
\▓▓▓▓▓▓ \▓▓         \▓▓           \▓▓▓▓▓▓ \▓▓
        </pre>
        <pre>
*****************************************************************
        </pre>
        <div class="infos">
            <p id="timer" class="timer">> Timer: 0s</p>
            <p id="date" class="date">> Thu Jan 1 00:00:00 CEST 1970</p>
            <p id="total" class="total">> Tokens: 0 (0$)</p>
        </div>
        <p class="typed-message"><?php echo $ip; ?>@gpt-87:~$ <span class="cursor">▋</span></p>
    </div>

    <form id="chat-form" method="POST">
        <input id="Input" class="input-green" type="text" name="user_input" placeholder="Prompt..." required>
        <button id="submit-button" class="button-green" onclick="playClick()" type="submit">Submit</button>
    </form>

    <script src="contents/js/script.js"></script>
    <script>
        const form = document.getElementById('configForm');
        const modelSelect = document.getElementById('model');
        const tempInput = document.getElementById('temperature');
        const freqInput = document.getElementById('frequencyPenalty');
        const presInput = document.getElementById('presencePenalty');

        tempInput.addEventListener('input', () => {
            const temperature = tempInput.value;
            const tempOutput = document.querySelector('.temperature-output');
            tempOutput.textContent = temperature;
        });
        freqInput.addEventListener('input', () => {
            const frequency = freqInput.value;
            const freqOutput = document.querySelector('.frequencyPenalty-output');
            freqOutput.textContent = frequency;
        });
        presInput.addEventListener('input', () => {
            const presence = presInput.value;
            const presOutput = document.querySelector('.presencePenalty-output');
            presOutput.textContent = presence;
        });

        // Fonction pour mettre à jour le fichier JSON
        function updateJSON() {
            const formData = new FormData(form);
            fetch('update_json.php', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                console.log(data.status);
                console.log(data.message);
            })
            .catch(error => console.error(error));
        }
        // Écouter les changements dans les curseurs et appeler la fonction de mise à jour
        form.addEventListener('change', updateJSON);

        // Variable pour Fn JS TypeWriter
        let ip = '<?php echo $ip; ?>';
        let model = '<?php echo $model; ?>';

        let counter = 0;
        let intervalId;

        function startCounter() {
          intervalId = setInterval(() => {
            counter++;
            document.getElementById('timer').textContent = '> Timer: '+counter+'s';
          }, 1000);
        }

        function stopCounter() {
          clearInterval(intervalId);
          counter = 0;
        }

        $(function() {
            $('#chat-form').submit(function(event) {
                event.preventDefault();

                let form = $(this);
                let url = form.attr('action');
                let method = form.attr('method');
                let data = form.serialize();

                document.getElementById('submit-button').disabled = true;
                startCounter();
                $('#timer').show();

                if (data !== 'user_input=') {
                    $.ajax({
                        url: url,
                        type: method,
                        data: data,
                        success: function(response) {
                          let created = response.created;
                          document.getElementById('date').textContent = '> '+created;
                          let promptTokens = response.prompt;
                          let completionTokens = response.completion;
                          let totalTokens = response.total;
                          let price = tokensPrice(promptTokens, completionTokens);
                          document.getElementById('total').textContent = '> Tokens: '+totalTokens+' ('+price+'$)';
                          stopCounter();
                          document.getElementById('submit-button').disabled = false;
                          typeWriterEffect(response.messages);
                          form[0].reset();
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            console.log('Error:', errorThrown);
                            stopCounter();
                            document.getElementById('submit-button').disabled = false;
                            alert('Error');
                            form[0].reset();
                        }
                    });
                    return false;
                } else {
                    alert('Prompt empty');
                }
            });
        });
    </script>
</body>
</html>
