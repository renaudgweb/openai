<?php
// Récupérer les valeurs des curseurs
$model = $_POST['model'];
$temperature = floatval($_POST['temperature']);
$frequency_penalty = floatval($_POST['frequencyPenalty']);
$presence_penalty = floatval($_POST['presencePenalty']);

// Créer un tableau avec les nouvelles valeurs
$data = array(
    'model' => $model,
    'temperature' => $temperature,
    'frequency_penalty' => $frequency_penalty,
    'presence_penalty' => $presence_penalty
);

// Convertir le tableau en format JSON
$json_data = json_encode($data);

// Écrire le JSON dans le fichier
$file = fopen('config.json', 'w');
fwrite($file, $json_data);
fclose($file);

// Répondre avec une confirmation
$response = array('status' => 'success', 'message' => 'Fichier JSON mis à jour avec succès');
echo json_encode($response);
?>
